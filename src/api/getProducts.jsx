async function getProducts() {
    let response = await fetch("/cards.json");
    let products = await response.json();
    return products
}

export default getProducts