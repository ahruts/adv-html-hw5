import React from "react";
import "./App.scss";
import Products from "./pages/Products";
import Cart from "./pages/Cart";
import Header from "./components/Header";
import { Route, Routes } from "react-router-dom";
import Favorites from "./pages/Favorites";

function App() {

  return (
    <>
      <Header />
      <Routes>
        <Route path="/products" element={<Products />}></Route>
        <Route path="/favorites" element={<Favorites />}></Route> */
        <Route path="/cart" element={<Cart />}></Route> */
      </Routes>
    </>
  );
}

export default App;
