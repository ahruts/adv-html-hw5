import { ErrorMessage, Field, Formik, Form} from "formik";
import React from "react";
import { number } from "yup";
import {object, string} from 'yup'

function CartForm({ onClick }) {

    const CustomErrorMessage = ({children}) => {
    return (<div style={{color: 'white', fontSize: '10px'}}>{children}</div>)
}
    const validationSchema = object({
        name: string().min(3, '*Too short name').required('*Required'),
        surname: string().min(3, '*Too short surname').required('*Required'),
        age: number().max(100, '*Age must me less than 100').required('*Required'),
        address: string().min(10, '*Too short address').required('*Required'),
        phone: number().required('*Required')
})

    return (
      <Formik
            initialValues={{ name: '', surname: '', age: '', address: '', phone: ''}}
            onSubmit={onClick}
            validationSchema={validationSchema}
        >
                <Form className="cart-form">
                    <div>
                    <label style={{ marginTop: 0 }} className="cart-form-label" htmlFor="name">Name</label>
                    <Field  name="name" id="name" className="cart-form-input" />
                    <ErrorMessage name="name" component={CustomErrorMessage}/>
                </div>
                <div>
                        <label className="cart-form-label" htmlFor="surname">Surname</label>
                    <Field name="surname" id="surname" className="cart-form-input" />
                    <ErrorMessage name="surname" component={CustomErrorMessage}/>
                </div>
                <div>
                        <label className="cart-form-label" htmlFor="age">Age</label>
                    <Field name="age" id="age" className="cart-form-input" />
                    <ErrorMessage name="age" component={CustomErrorMessage}/>
                </div>
                <div>
                        <label className="cart-form-label" htmlFor="address">Address</label>
                    <Field name="address" id="address" className="cart-form-input" />
                    <ErrorMessage name="address" component={CustomErrorMessage}/>
                </div>
                <div>
                    <label className="cart-form-label" htmlFor="phone">Phone number</label>
                    <Field name="phone" id="phone" className="cart-form-input" />
                    <ErrorMessage name="phone" component={CustomErrorMessage}/>
                    </div>
                    <div>
                        <button type="submit">Checkout</button>
                    </div>
                </Form>
            </Formik>
    );
}

export default CartForm;