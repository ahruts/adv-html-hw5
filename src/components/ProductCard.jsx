import React, { useState } from "react";
import { FaRegStar, FaStar } from 'react-icons/fa';
import PropTypes from 'prop-types';
import { useDispatch } from 'react-redux'
import { addToFavoritesAction, deleteFromFavoritesAction } from "../store/actions";


function ProductCard ({vendorCode, favorite, url, color, price, name, openModal, buttonText}) {
  const [favorites, setFavorite] = useState();
    const dispatch = useDispatch()


  const deleteFromFavorites = () => {
    dispatch(deleteFromFavoritesAction(vendorCode))
    if (typeof JSON.parse(localStorage.getItem("favorites")) == 'number') {
      localStorage.removeItem(`favorites`)
    } else {
      let favorites = JSON.parse(localStorage.getItem("favorites"))
      const index = favorites.indexOf(vendorCode);
      favorites.splice(index, 1);
      localStorage.setItem('favorites', `${JSON.stringify(favorites)}`)
    }
  }

  const addToFavorites = () => {
    dispatch(addToFavoritesAction(vendorCode))
    if (typeof JSON.parse(localStorage.getItem("favorites")) == 'object' && localStorage.getItem("favorites")) {
      let favorites = JSON.parse(localStorage.getItem("favorites"))
      favorites.push(`${[vendorCode]}`)
      localStorage.setItem('favorites', `${JSON.stringify(favorites)}`)
    } else if (typeof JSON.parse(localStorage.getItem("favorites")) == 'number') {
      let favorites = localStorage.getItem('favorites').split(' ')
      favorites.push(`${[vendorCode]}`)
      localStorage.setItem('favorites', `${JSON.stringify(favorites)}`)
    } else if (typeof JSON.parse(localStorage.getItem("favorites")) == 'object' && !localStorage.getItem("favorites")) {
      localStorage.setItem('favorites', `${[vendorCode]}`)
    }
  }


    return (
      <div className="product-card">
        <img src={url} alt="" />
          <h1 className="product-card-name">
            {name}
          </h1>
            <p className="product-card-color">{color}</p>
            <h2 className="product-card-price">{price}</h2>
        <button className="product-card-btn" onClick={event => openModal(vendorCode)}>{buttonText}</button>
        {favorites || (localStorage.getItem(`favorites`) && localStorage.getItem(`favorites`).includes(vendorCode)) ? <FaStar onClick={deleteFromFavorites}/> : <FaRegStar onClick={addToFavorites}/>}
      </div>
    );
}

ProductCard.defaultProps = {
  name: 'product',
  price: "100",
  color: 'natural',
};

ProductCard.propTypes = {
  name: PropTypes.string,
  color: PropTypes.string,
  price: PropTypes.string,
  url: PropTypes.string,
  favorite: PropTypes.bool,
  openModal: PropTypes.func,
};

export default ProductCard;