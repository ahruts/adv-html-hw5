import React from "react";
import { NavLink } from "react-router-dom";

function Header({ closeModal }) {
    return (
      <div className="header" onClick={closeModal}>
            <NavLink className='header-btn' style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/products">Products</NavLink>
            <NavLink className='header-btn' style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/favorites">Favorites</NavLink>
            <NavLink className='header-btn' style={({isActive}) => isActive ? {fontWeight: 'bold'} : undefined} to="/cart">Cart</NavLink>
      </div>
    );
}

export default Header