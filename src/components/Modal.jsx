import propTypes from "prop-types";
import React from "react";

function Modal({closeModal, confirmModal, cancelModal, id, text}) {
    return (
      <div className="modal" onClick={closeModal}>
        <div className="modal-content" onClick={(e) => e.stopPropagation()}>
          <p>{text}</p>
          <div className="modal-btn-container">
              <button id={id}
                className="modal-btn"
                onClick={confirmModal}
          >Yes</button>
          <button
                className="modal-btn"
                onClick={cancelModal}
            >No</button>
            </div>
        </div>
      </div>
    );
}

Modal.propTypes = {
  id: propTypes.string,
  confirmModal: propTypes.func,
  cancelModal: propTypes.func,
};

export default Modal;