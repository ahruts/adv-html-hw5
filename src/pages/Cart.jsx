import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import ProductCard from "../components/ProductCard";
import { useDispatch, useSelector } from 'react-redux'
import { deleteFromCartAction, getProductsAction, submitCartForm, toggleModalAction } from "../store/actions";
import CartForm from "../components/CartForm";

function Cart() {
  const [catalog, setCatalog] = useState(null);
  const [modal, setModal] = useState(null);
  const dispatch = useDispatch()
  const stateCards = useSelector((state) => state.cards);

  useEffect(() => {
  cartProducts()
    }, [stateCards]);

  const cancelModal = () => {
    dispatch(toggleModalAction(false))
    setModal(null)
  };

  const submitForm = (values, actions) => {
    const submitData = {
      name: values.name,
      age: values.age,
      address: values.address,
phone: values.phone,
    }
    dispatch(submitCartForm(submitData))
  };

  const confirmModal = (event) => {
    if (typeof JSON.parse(localStorage.getItem("cart")) == 'number') {
      localStorage.removeItem(`cart`)
    } else {
      let cart = JSON.parse(localStorage.getItem("cart"))
      const index = cart.indexOf(event.currentTarget.id);
      cart.splice(index, 1);
      localStorage.setItem('cart', `${JSON.stringify(cart)}`)
    }
    dispatch(toggleModalAction(false))
    dispatch(deleteFromCartAction(event.currentTarget.id))
    setModal(null)
  };

  const openModal = (id) => {
    dispatch(toggleModalAction(true))
    setModal(
      <Modal id={id} cancelModal={cancelModal} confirmModal={confirmModal} text={'Remove from cart?'}/>
    );
  };

  function cartProducts() {
    let cleanupFunction = false;
      if (!stateCards) {
      dispatch(getProductsAction())
      }
      if (stateCards) {
        const cart = stateCards.filter((item) => {
          if (item.cart === true) {
            return item;
          } else return false;
        });
        const catalog = cart.map((item) => {
          return (
            <ProductCard
              key={item.vendorCode}
              url={item.URL}
              name={item.name}
              price={item.price}
              color={item.color}
              vendorCode={item.vendorCode}
              openModal={openModal}
              favorite={item.favorite}
              buttonText={"delete"}
              favoritesPage={true}
            ></ProductCard>
          );
        });
        if (!cleanupFunction) {
          setCatalog(catalog)
        };
    }
    return () => cleanupFunction = true;
  }

    return (
      <>
        <div className="product-card-list">
          {catalog ? catalog : <p>Loading..</p>}
        </div>{" "}
        <CartForm onClick={submitForm}>
        </CartForm>
        {modal}
      </>
    );
}

export default Cart