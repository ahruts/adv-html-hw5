import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import ProductCard from "../components/ProductCard";
import { useDispatch, useSelector } from 'react-redux'
import { addToCartAction, getProductsAction, toggleModalAction } from "../store/actions";

function Favorites() {
  const [catalog, setCatalog] = useState(null);
  const [modal, setModal] = useState(null);
  const dispatch = useDispatch()
  const stateCards = useSelector((state) => state.cards);

  useEffect(() => {
favoritesList()
  }, [stateCards]);

  const cancelModal = () => {
    dispatch(toggleModalAction(false))
    setModal(null)
  };

  const confirmModal = (event) => {
    if (typeof JSON.parse(localStorage.getItem("cart")) == 'object' && localStorage.getItem("cart")) {
      let cart = JSON.parse(localStorage.getItem("cart"))
      if (!cart.includes(`${[event.currentTarget.id]}`)) {
      cart.push(`${[event.currentTarget.id]}`)
      localStorage.setItem('cart', `${JSON.stringify(cart)}`)
      }
    } else if (typeof JSON.parse(localStorage.getItem("cart")) == 'number') {
      let cart = localStorage.getItem('cart').split(' ')
      if (!cart.includes(`${[event.currentTarget.id]}`)) {
      cart.push(`${[event.currentTarget.id]}`)
      localStorage.setItem('cart', `${JSON.stringify(cart)}`)
      }
    } else if (typeof JSON.parse(localStorage.getItem("cart")) == 'object' && !localStorage.getItem("cart")) {
      localStorage.setItem('cart', `${[event.currentTarget.id]}`)
    }
    setModal(null)
    dispatch(toggleModalAction(false))
    dispatch(addToCartAction(event.currentTarget.id))
  };

  const openModal = (id) => {
    dispatch(toggleModalAction(true))
    setModal(
      <Modal id={id} cancelModal={cancelModal} confirmModal={confirmModal} text={'Add to the cart?'} />
    );
  };

  function favoritesList() {
    let cleanupFunction = false;
      if (!stateCards) {
      dispatch(getProductsAction())
      }
      if (stateCards) {
          const favorites = stateCards.filter((item) => {
          if (item.favorite === true) {
              return item;
          } else {
            return false;
        }
      });
      const catalog = favorites.map((item) => {
      return (
        <ProductCard
          key={item.vendorCode}
          url={item.URL}
          name={item.name}
          price={item.price}
          color={item.color}
          vendorCode={item.vendorCode}
          openModal={openModal}
              favorite={item.favorite}
              buttonText={"ADD TO CART"}
              favoritesPage={true}
        ></ProductCard>
      );
      });
        if (!cleanupFunction) { setCatalog(catalog) };
    }
    return () => cleanupFunction = true;
  }

    return (
      <>
        <div className="product-card-list">
          {catalog ? catalog : <p>Loading..</p>}
        </div>{" "}
        {modal}
      </>
    );
}

export default Favorites