import { useEffect, useState } from "react";
import Modal from "../components/Modal";
import ProductCard from "../components/ProductCard";
import { useDispatch, useSelector } from 'react-redux'
import { addToCartAction, getProductsAction, toggleModalAction } from "../store/actions";

function Products() {
  const [catalog, setCatalog] = useState(null);
  const [modal, setModal] = useState(null);
  const dispatch = useDispatch()
  const stateCards = useSelector((state) => state.cards);

  useEffect(() => {
    productsList()
  }, [stateCards]);

  const cancelModal = () => {
    setModal(null)
    dispatch(toggleModalAction(false))
  };

  const confirmModal = (event) => {
    if (typeof JSON.parse(localStorage.getItem("cart")) == 'object' && localStorage.getItem("cart")) {
      let cart = JSON.parse(localStorage.getItem("cart"))
      if (!cart.includes(`${[event.currentTarget.id]}`)) {
      cart.push(`${[event.currentTarget.id]}`)
      localStorage.setItem('cart', `${JSON.stringify(cart)}`)
      }
    } else if (typeof JSON.parse(localStorage.getItem("cart")) == 'number') {
      let cart = localStorage.getItem('cart').split(' ')
      if (!cart.includes(`${[event.currentTarget.id]}`)) {
      cart.push(`${[event.currentTarget.id]}`)
      localStorage.setItem('cart', `${JSON.stringify(cart)}`)
      }
    } else if (typeof JSON.parse(localStorage.getItem("cart")) == 'object' && !localStorage.getItem("cart")) {
      localStorage.setItem('cart', `${[event.currentTarget.id]}`)
    }
    dispatch(addToCartAction(event.currentTarget.id))
    setModal(null)
    dispatch(toggleModalAction(false))
  };

  const openModal = (id) => {
     dispatch(toggleModalAction(true))
    setModal(
      <Modal id={id} cancelModal={cancelModal} confirmModal={confirmModal} text={'Add to the cart?'} />
    );
  };

  function productsList() {
    if (!stateCards) {
      dispatch(getProductsAction())
    }
    if (stateCards) {
      const catalog = stateCards.map((item) => {
        return (
          <ProductCard
            key={item.vendorCode}
            url={item.URL}
            name={item.name}
            price={item.price}
            color={item.color}
            vendorCode={item.vendorCode}
            openModal={openModal}
            favorite={item.favorite}
            buttonText={"ADD TO CART"}
          ></ProductCard>
        );
      });
      setCatalog(catalog)
    }
}

    return (
      <>
        <div className="product-card-list">
          {catalog ? catalog : <p>Loading..</p>}
        </div>
        {modal}
      </>
    );
}

export default Products